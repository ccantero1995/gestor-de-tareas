from django.shortcuts import redirect, render

from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate
from django.contrib.auth import logout, login
from django.contrib.auth.models import User, Group
from Tareas.models import *
import pdb

def inicio(request):

	menu =  {
				'Inicio'    : '#inicio',
				'Inicio sesion'  : '#sesion', 
				'Registro'  : '#registro', 
			}

	template = loader.get_template('Tareas/inicio.html')
	return HttpResponse(template.render({ 'menu':menu }, request))

def registrar(request):

	username = request.POST["usuario"]
	email    = request.POST["correo"]
	password = request.POST["clave"]

	if User.objects.filter(username = username).exists():
		tipo    = "Registro invalido"
		mensaje = "El nombre de usuario ya esta registrado, intente con otro"
	elif User.objects.filter(email = email).exists(): 
		tipo    = "Registro invalido"
		mensaje = "El correo ya esta registrado, intente con otro"
	else:

		user = User(username = username, email = email)
		user.set_password(password)
		user.save()

		tipo    = "Registro exitoso"
		mensaje = "El usuario, ha sido creado exitosamente"

	template = loader.get_template('Tareas/registrar.html')
	return HttpResponse(template.render({ 'mensaje':mensaje, 'tipo':tipo }, request))

def sesion(request):

	menu    = {
				"Salir":"./salir/",
				"Tareas finalizadas": "./finalizada/",
				"Tareas en curso": "./enCurso/"
			  }
	template = 'Tareas/sesion.html'

	if not request.user.is_authenticated:
		usuario = authenticate(username = request.POST['usuario'], password = request.POST['clave'])
		if (usuario is not None) and (usuario.is_active):
					
			logout(request)
			login(request, usuario)

		else:
			template = 'Tareas/errorInicio.html'

	return render(request,template,{"menu":menu})

def crearTarea(request):

	tarea = tareasModel()

	tarea.user        = User.objects.get(username = request.user.get_username())
	tarea.titulo      = request.POST["titulo"]
	tarea.descripcion = request.POST["descripcion"]
	tarea.detalle     = request.POST["detalle"]

	tarea.save()

	return redirect("/Tareas/inicio/sesion/")

def salir(request):
	
	logout(request)
	return redirect("/Tareas/inicio/")

def finalizado(request):
	menu    = {
				"Salir":"./salir/",
				"Inicio":"./inicio/"
			  }
	user   = User.objects.get(username = request.user.get_username()) 
	tareas = tareasModel.objects.filter(user = user, estado = True)
	
	return render(request,"Tareas/tareasFinalizadas.html",{"menu":menu, "tareas": tareas})

def curso(request):
	menu    = {
				"Salir":"./salir/",
				"Inicio":"./inicio/"
			  }
	user   = User.objects.get(username = request.user.get_username()) 
	tareas = tareasModel.objects.filter(user = user, estado = False)
	
	return render(request,"Tareas/tareasPendientes.html",{"menu":menu, "tareas": tareas})

def finalizar(request, ID):

	try:

		user  = User.objects.get(username = request.user.get_username())
		tarea = tareasModel.objects.get(ID = ID, user = user)

		tarea.estado = True
		tarea.save()

	except:
		pass

	return redirect("/Tareas/inicio/sesion/enCurso/")

def alInicio(request):
	return redirect("/Tareas/inicio/sesion/")

def error_404_view(request, exception):
    return render(request,'404.html', {})