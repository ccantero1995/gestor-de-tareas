from django.db import models
from django.contrib.auth.models import User

class tareasModel(models.Model):
	ID           = models.AutoField(primary_key=True)
	user         = models.ForeignKey(User, on_delete=models.CASCADE)
	titulo       = models.CharField(max_length= 15)
	descripcion  = models.CharField(max_length= 30)
	detalle      = models.TextField()
	estado       = models.BooleanField(default = False)