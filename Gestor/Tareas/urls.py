from django.urls import path
from Tareas import views

urlpatterns = [
  path('inicio/', views.inicio),
  path('inicio/registrar/', views.registrar),
  path('inicio/sesion/', views.sesion),
  path('inicio/sesion/crearTarea/', views.crearTarea),
  path('inicio/sesion/salir/', views.salir),
  path('inicio/sesion/finalizada/', views.finalizado),
  path('inicio/sesion/finalizada/salir/', views.finalizado),
  path('inicio/sesion/enCurso/', views.curso),
  path('inicio/sesion/enCurso/salir/', views.curso),
  path('inicio/sesion/finalizada/inicio/', views.alInicio),
  path('inicio/sesion/enCurso/inicio/', views.alInicio),
  path('inicio/sesion/enCurso/finalizar/<int:ID>/', views.finalizar),
]