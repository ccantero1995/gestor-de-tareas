HOST = "localhost:8000/SIGEUC/inicio/"

var formEgresado = document.forms.namedItem("formEgresado");
formEgresado.addEventListener('submit', function(ev) {

  ev.preventDefault();
  var oReq = new XMLHttpRequest();
  oReq.open("POST", HOST + "egresado", true).send();

}, false);

var formValidador = document.forms.namedItem("formValidador");
formValidador.addEventListener('submit', function(ev) {

  ev.preventDefault();
  var oReq = new XMLHttpRequest();
  oReq.open("POST", HOST + "validador", true).send();

}, false);

var formDigae = document.forms.namedItem("formDigae");
formDigae.addEventListener('submit', function(ev) {

  ev.preventDefault();
  var oReq = new XMLHttpRequest();
  oReq.open("POST", HOST + "digae", true).send();

}, false);